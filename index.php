<?php 
require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');


$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo $sheep->legs; // 2
echo $sheep->cold_blooded; // false


// index.php
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->legs = 4;
$kodok->jump() ; // "hop hop"
echo $kodok->legs;
?>